import {Component, OnInit, ViewChild} from '@angular/core';
import {SearchService} from './search.service';
import {AlbumModel} from './album.model';
import {forkJoin} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {map} from 'rxjs/operators';
import {FormComponent} from './form/form.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  notFound = false;
  query: string;
  loading = false;

  albums: AlbumModel[] = [];

  constructor(private searchService: SearchService,
              private router: Router,
              private route: ActivatedRoute) {
  }

  @ViewChild(FormComponent, {static: false}) formComponent: FormComponent;


  ngOnInit() {
    this.route.queryParamMap
      .pipe(map(params => params.get('artist')))
      .subscribe(param => {
        this.query = param;
        if (this.query) {
          this.onSearch(this.query);
          this.formComponent.form.controls.artist.setValue(this.query);
        }
      });
  }


  onSearch(artist: string) {
    this.albums = [];
    this.loading = true;
    this.notFound = false;
    this.router.navigate([''], {queryParams: {artist}});
    this.searchService.getData(artist)
      .subscribe(albums => {
        this.loading = false;
        this.albums = albums;
        if (!this.albums.length) {
          this.notFound = true;
          this.query = artist;
        }
      });
  }
}
