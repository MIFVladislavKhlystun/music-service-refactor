export interface ItunesResult {
  artistName: string;
  collectionName: string;
  artworkUrl100: string;
  trackCount: number;
  collectionViewUrl: string;
}
