import { DeezerResult } from './deezer-result';

export interface Deezer {
  data: DeezerResult[];
}
