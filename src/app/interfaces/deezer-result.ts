import { Artist } from './artist';

export interface DeezerResult {
  artist: Artist;
  title: string;
  cover_big: string;
  nb_tracks: number;
  link: string;
}
