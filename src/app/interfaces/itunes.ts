import { ItunesResult } from './itunes-result';

export interface Itunes {
  results: ItunesResult[];
}
