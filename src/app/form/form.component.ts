import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  form: FormGroup;

  @Output() search = new EventEmitter<string>();


  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.form = this.fb.group({
      artist: ['', Validators.required]
    });
  }


  onSubmit() {
    this.search.emit(this.form.value.artist);
  }

}
