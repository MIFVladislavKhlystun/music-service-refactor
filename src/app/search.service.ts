import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {forkJoin} from 'rxjs';
import {Deezer} from './interfaces/deezer';
import {Itunes} from './interfaces/itunes';
import {AlbumModel} from './album.model';


@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor(private http: HttpClient) {
  }

  getDeezerData(artist: string) {
    const receivedAlbums = [];
    return this.http.jsonp<Deezer>(`https://api.deezer.com/search/album/?q=${artist}&output=jsonp`, 'callback')
      .pipe(map(result => {
          result.data.map((album) => {
            receivedAlbums.push({
              title: album.title,
              albumImage: album.cover_big,
              tracksCount: album.nb_tracks,
              link: album.link,
              artistName: album.artist.name,
            });
          });
          return receivedAlbums;
        })
      );
  }

  getItunesData(artist: string) {
    const receivedAlbums = [];
    const URL = 'https://itunes.apple.com/search';
    const params = new HttpParams()
      .set('term', artist)
      .set('entity', 'album');
    return this.http.get<Itunes>(URL, {params})
      .pipe(map(result => {
          result.results.map((album) => {
            receivedAlbums.push({
              title: album.collectionName,
              albumImage: album.artworkUrl100,
              tracksCount: album.trackCount,
              link: album.collectionViewUrl,
              artistName: album.artistName
            });
          });
          return receivedAlbums;
        })
      );
  }

  uniq(albums: AlbumModel[], param: string) {
    return albums.filter((item, pos, array) => {
      return array.map(mapItem => mapItem[param]).indexOf(item[param]) === pos;
    });
  }

  getData(artist: string) {
    return forkJoin(this.getDeezerData(artist), this.getItunesData(artist))
      .pipe(map(res => {
          let albums: AlbumModel[];
          albums = res[0].concat(res[1]);
          return this.uniq(albums, 'title');
        })
      );
  }
}
