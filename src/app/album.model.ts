export class AlbumModel {
  artistName: string;
  title: string;
  albumImage: string;
  tracksCount: number;
  link: string;
}
